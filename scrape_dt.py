from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
from urllib.request import urlopen
from datetime import datetime
import xml.etree.cElementTree as ET
import re
import json

def retrieve_page(url):
    """Retrive the homepage of Dhaka Tribune and save it in output.html.
    """
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    driver = webdriver.Chrome(chrome_options=options)
    driver.get(url)
    try:
        WebDriverWait(driver, 60).until(
            EC.visibility_of_element_located((By.CLASS_NAME, 'dt-strem-new')))
    finally:
        pageSource = driver.page_source
        bs = BeautifulSoup(pageSource, 'html.parser')
        html = bs.prettify("utf-8")
        htmlName = "output.html"
        with open(htmlName, "wb") as file:
            file.write(html)
        file.close()
        return htmlName

def format_to_xmldate(dateText):
    """Convert published date to xml format for pubDate
    03:00 pm September 3rd, 2018 incorrect
    Mon, 03 Sep 2018 15:16:07 +0600 correct"""
    patternText = r"(\d\d:\d\d .m) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\w* (\d*)\w\w, (\d\d\d\d)"
    pattern = re.compile(patternText)
    s = pattern.match(dateText)
    time12h, month, dayNumber, year = s.groups()
    dayNumber = (dayNumber if len(dayNumber) == 2 else '0' + dayNumber)

    # Create datetime obj to find the weekday as 3-letter word
    dayWord = datetime.strptime('%s-%s-%s' % (dayNumber, month, year), '%d-%b-%Y').strftime('%A')[:3]
    # Create datetime obj to find the 24h time
    time = (datetime.strptime(time12h, "%I:%M %p")).strftime('%H:%M:%S')

    return '%s, %s %s %s %s' % (dayWord, dayNumber, month, year, time)

def format_to_jsondate(dateText):
    """Convert date:
    03:00 pm September 3rd, 2018 incorrect
    2018-09-18T18:18:40 correct"""
    patternText = r"(\d\d:\d\d .m) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\w* (\d*)\w\w, (\d\d\d\d)"
    pattern = re.compile(patternText)
    s = pattern.match(dateText)
    time12h, month, dayNumber, year = s.groups()
    dayNumber = (dayNumber if len(dayNumber) == 2 else '0' + dayNumber)

    # Create datetime obj to find the weekday as 3-letter word
    date = datetime.strptime('%s-%s-%s' % (dayNumber, month, year), '%d-%b-%Y').strftime('%Y-%m-%d')
    # Create datetime obj to find the 24h time
    time = (datetime.strptime(time12h, "%I:%M %p")).strftime('%H:%M:%S')

    return '%sT%s' % (date, time)

def retrieve_article_detail(articleUrl):
    """Get description and publication date of an article by
    visiting articleUrl"""
    html = urlopen(articleUrl)
    bs = BeautifulSoup(html.read(), "html.parser")
    desc = bs.find('div', {'class': 'highlighted-content'}).p.get_text()
    author = bs.find('div', {'class': 'ptt author-bg'}).find('a').get_text().strip('\n')
    featuredImage = bs.find('div', {'class': 'reports-big-img details-reports-big-img'}).img['src']
    pubDate = bs.find('div', {'class': 'detail-poauthor'}).find('li').get_text()
    pubDate = pubDate.strip("\n 'Published at'")
    # pubDate = format_to_xmldate(pubDate)
    return {'date': pubDate, 'shortDescription': desc[:50], 
    'description': desc, 'author': author, 'featuredImage': featuredImage,
    'id': articleUrl, 'thumbnails': ''}

def retrieve_headlines(url, htmlName):
    html = open(htmlName, "rb")
    bs = BeautifulSoup(html, 'html.parser')
    headlines = bs.find('ul', {'class': 'just_in_news'}).find_all('h2')
    j = []
    # print(bs.find('ul', {'class': 'just_in_news'}))
    for headline in headlines:
        link = url.strip('/') + headline.a['href'] # Try urljoin?
        j.append({'title': headline.a.get_text().strip('\n '), 'link': link})
    html.close()
    return j

def retrieve_articles(url, htmlName):
    articles = retrieve_headlines(url, htmlName)
    for article in articles:
        descAndPub = retrieve_article_detail(article['link'])
        article = article.update(descAndPub)
    return articles

def make_rss(articleDict):
    rss = ET.Element("rss", version="2.0")
    channel = ET.SubElement(rss, "channel")

    ET.SubElement(channel, "title").text = "Dhaka Tribune"
    ET.SubElement(channel, "link").text = "https://www.dhakatribune.com/"
    ET.SubElement(channel, "description").text = "Dhaka Tribune latest news"
    ET.SubElement(channel, "lastBuildDate").text = str(datetime.now().strftime('%a, %B %M %Y'))

    for article in articleDict:
        item = ET.SubElement(channel, "item")
        ET.SubElement(item, "title").text = article['title']
        ET.SubElement(item, "link").text = article['link']
        ET.SubElement(item, "guid").text = article['link']
        ET.SubElement(item, "description").text = article['description']
        ET.SubElement(item, "pubDate").text = format_to_xmldate(article['date'])

    tree = ET.ElementTree(rss)
    tree.write("dt.xml", encoding='utf-8', xml_declaration=True)

def make_json(articles):
    """Make a .json file from a dictionary of articles.
    """
    for article in articles:
        article['date'] = format_to_jsondate(article['date'])
    with open('dt.json', 'w') as outfile:
        json.dump(articles, outfile)

url = "https://www.dhakatribune.com/"
htmlName = retrieve_page(url)
# htmlName = 'output.html'
articles = retrieve_articles(url, htmlName)
# make_rss(articles)
make_json(articles)

# dets = retrieve_article_detail("https://www.dhakatribune.com/opinion/op-ed/2018/09/19/it-s-time-for-bangladesh-to-play-the-game")
# dets['date'] = format_to_jsondate(dets['date'])
# dets['id'] = dets['link']
# with open('extra.json', 'w') as outfile:
#     json.dump(dets, outfile)
